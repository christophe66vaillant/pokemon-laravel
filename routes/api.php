<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/pokedex', 'ApiPokedexController@index');
Route::post('/pokemons/save', 'ApiPokemonController@store');
Route::post('/player/save', 'ApiPlayerController@store');
Route::get('/player/{user}/info', 'ApiPlayerController@show');
Route::get('/pokemons/types', 'ApiPokemonTypeController@index');
Route::get('/events', 'ApiEventController@index');
Route::post('/event/create', 'ApiEventController@store');