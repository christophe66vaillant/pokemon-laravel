-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.6.38 - MySQL Community Server (GPL)
-- SE du serveur:                osx10.9
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour pokemon_laravel
DROP DATABASE IF EXISTS `pokemon_laravel`;
CREATE DATABASE IF NOT EXISTS `pokemon_laravel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `pokemon_laravel`;

-- Listage de la structure de la table pokemon_laravel. events
DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` bigint(20) unsigned NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `events_type_id_foreign` (`type_id`),
  CONSTRAINT `events_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.events : ~2 rows (environ)
DELETE FROM `events`;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `type_id`, `start_time`, `end_time`) VALUES
	(5, 1, '2019-03-14 00:00:00', '2019-03-22 00:00:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.migrations : ~10 rows (environ)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 2),
	(3, '2019_03_26_123150_create_pokemon_table', 3),
	(4, '2019_03_26_130114_create_types_table', 4),
	(5, '2019_03_26_130159_create_events_table', 5),
	(6, '2019_03_26_134116_create_pokemon_user_table', 6),
	(7, '2019_03_26_134229_create_pokemon_type_table', 7),
	(8, '2019_03_26_134332_update_pokemon_type_table', 8),
	(9, '2019_03_26_135044_update_pokemon_user_table', 9),
	(10, '2019_03_26_142403_update_event_table', 10);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.password_resets : ~0 rows (environ)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. pokemon
DROP TABLE IF EXISTS `pokemon`;
CREATE TABLE IF NOT EXISTS `pokemon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `evolution_id` bigint(20) NOT NULL,
  `bgPosY` int(10) NOT NULL,
  `bgPosX` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.pokemon : ~36 rows (environ)
DELETE FROM `pokemon`;
/*!40000 ALTER TABLE `pokemon` DISABLE KEYS */;
INSERT INTO `pokemon` (`id`, `name`, `evolution_id`, `bgPosY`, `bgPosX`) VALUES
	(1, 'Bulbizarre', 1, 0, 0),
	(2, 'Herbizarre', 2, 0, -100),
	(3, 'Florizarre', 3, 0, -200),
	(4, 'Salamèche', 1, 0, -300),
	(5, 'Reptincel', 2, 0, -400),
	(6, 'Dracaufeu', 3, 0, -500),
	(7, 'Carapuce', 1, 0, -600),
	(8, 'Carabaffe', 2, 0, -700),
	(9, 'Tortank', 3, 0, -800),
	(10, 'Chenipan', 1, 0, -900),
	(11, 'Chrysacier', 2, 0, -1000),
	(12, 'Papilusion', 3, 0, -1100),
	(13, 'Aspicot', 1, 0, -1200),
	(14, 'Coconfort', 2, 0, -1300),
	(15, 'Dardargnan', 3, 0, -1400),
	(16, 'Roucool', 1, 0, -1500),
	(17, 'Roucoups', 2, 0, -1600),
	(18, 'Roucarnage', 3, 0, -1700),
	(19, 'Rattata', 1, 0, -1800),
	(20, 'Rattatac', 2, 0, -1900),
	(21, 'Piafabec', 1, 0, -2000),
	(22, 'Rapasdepic', 2, 0, -2100),
	(23, 'Abo', 1, 0, -2200),
	(24, 'Arbok', 2, 0, -2300),
	(25, 'Pikachu', 1, 0, -2400),
	(26, 'Raichu', 2, -100, 0),
	(27, 'Sablette', 1, -100, -100),
	(28, 'Sablaireau', 2, -100, -200),
	(29, 'Nidoran♀', 1, -100, -300),
	(30, 'Nidorina', 2, -100, -400),
	(31, 'Nidoqueen', 3, -100, -500),
	(32, 'Nidoran♂', 1, -100, -600),
	(33, 'Nidorino', 2, -100, -700),
	(34, 'Nidoking', 3, -100, -800),
	(35, 'Mélofée', 1, -100, -900),
	(36, 'Mélodelfe', 2, -100, -1000);
/*!40000 ALTER TABLE `pokemon` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. pokemon_type
DROP TABLE IF EXISTS `pokemon_type`;
CREATE TABLE IF NOT EXISTS `pokemon_type` (
  `type_id` bigint(20) unsigned NOT NULL,
  `pokemon_id` bigint(20) unsigned NOT NULL,
  KEY `pokemon_type_type_id_foreign` (`type_id`),
  KEY `pokemon_type_pokemon_id_foreign` (`pokemon_id`),
  CONSTRAINT `pokemon_type_pokemon_id_foreign` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pokemon_type_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.pokemon_type : ~51 rows (environ)
DELETE FROM `pokemon_type`;
/*!40000 ALTER TABLE `pokemon_type` DISABLE KEYS */;
INSERT INTO `pokemon_type` (`type_id`, `pokemon_id`) VALUES
	(1, 1),
	(6, 1),
	(1, 2),
	(6, 2),
	(1, 3),
	(6, 3),
	(2, 4),
	(2, 5),
	(2, 6),
	(5, 6),
	(3, 7),
	(3, 8),
	(3, 9),
	(4, 10),
	(4, 11),
	(4, 12),
	(5, 12),
	(4, 13),
	(6, 13),
	(4, 14),
	(6, 14),
	(4, 15),
	(6, 15),
	(5, 16),
	(7, 16),
	(5, 17),
	(7, 17),
	(5, 18),
	(7, 18),
	(7, 19),
	(7, 20),
	(5, 21),
	(7, 21),
	(5, 22),
	(7, 22),
	(6, 23),
	(6, 24),
	(8, 25),
	(8, 26),
	(9, 27),
	(9, 28),
	(8, 29),
	(6, 30),
	(6, 31),
	(9, 31),
	(8, 32),
	(6, 33),
	(6, 34),
	(9, 34),
	(10, 35),
	(10, 36);
/*!40000 ALTER TABLE `pokemon_type` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. pokemon_user
DROP TABLE IF EXISTS `pokemon_user`;
CREATE TABLE IF NOT EXISTS `pokemon_user` (
  `user_id` bigint(20) unsigned NOT NULL,
  `pokemon_id` bigint(20) unsigned NOT NULL,
  `PC` bigint(20) NOT NULL,
  KEY `pokemon_user_user_id_foreign` (`user_id`),
  KEY `pokemon_user_pokemon_id_foreign` (`pokemon_id`),
  CONSTRAINT `pokemon_user_pokemon_id_foreign` FOREIGN KEY (`pokemon_id`) REFERENCES `pokemon` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pokemon_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.pokemon_user : ~147 rows (environ)
DELETE FROM `pokemon_user`;
/*!40000 ALTER TABLE `pokemon_user` DISABLE KEYS */;
INSERT INTO `pokemon_user` (`user_id`, `pokemon_id`, `PC`) VALUES
	(1, 1, 100),
	(1, 1, 100),
	(1, 1, 962),
	(1, 1, 962),
	(1, 1, 962),
	(1, 22, 665),
	(1, 9, 2131),
	(1, 9, 2131),
	(1, 9, 2131),
	(1, 35, 759),
	(1, 35, 759),
	(1, 35, 759),
	(1, 8, 1476),
	(1, 10, 272),
	(1, 17, 520),
	(1, 32, 239),
	(1, 11, 1219),
	(1, 10, 775),
	(1, 2, 1761),
	(1, 30, 401),
	(1, 11, 650),
	(1, 30, 401),
	(1, 6, 1858),
	(1, 28, 1619),
	(1, 28, 1619),
	(1, 11, 650),
	(1, 11, 650),
	(1, 31, 2519),
	(1, 28, 515),
	(1, 4, 322),
	(1, 1, 1),
	(1, 11, 801),
	(1, 36, 498),
	(1, 25, 99),
	(1, 7, 369),
	(1, 31, 292),
	(1, 18, 1698),
	(1, 5, 1101),
	(1, 22, 278),
	(1, 28, 1345),
	(1, 26, 1747),
	(1, 24, 1496),
	(1, 24, 1363),
	(1, 5, 1327),
	(1, 5, 1628),
	(1, 23, 267),
	(1, 14, 1223),
	(1, 27, 219),
	(1, 8, 313),
	(1, 21, 523),
	(1, 1, 898),
	(1, 1, 829),
	(1, 23, 19),
	(1, 25, 466),
	(1, 8, 988),
	(1, 35, 569),
	(1, 19, 155),
	(1, 30, 1635),
	(1, 25, 318),
	(1, 13, 731),
	(1, 17, 808),
	(1, 32, 591),
	(1, 18, 464),
	(1, 1, 891),
	(1, 8, 585),
	(1, 10, 321),
	(1, 24, 1021),
	(1, 8, 330),
	(1, 31, 875),
	(1, 5, 1776),
	(1, 2, 303),
	(1, 19, 707),
	(1, 6, 1835),
	(1, 35, 846),
	(1, 7, 836),
	(1, 35, 14),
	(1, 9, 2893),
	(1, 30, 1567),
	(1, 8, 859),
	(1, 22, 1293),
	(1, 15, 775),
	(1, 14, 631),
	(1, 20, 777),
	(1, 31, 2482),
	(1, 9, 2872),
	(1, 25, 650),
	(1, 13, 997),
	(1, 22, 473),
	(1, 1, 768),
	(1, 4, 737),
	(1, 29, 50),
	(1, 4, 471),
	(1, 23, 280),
	(1, 30, 729),
	(1, 2, 946),
	(1, 20, 1900),
	(1, 16, 229),
	(1, 14, 1401),
	(1, 16, 295),
	(1, 6, 2682),
	(1, 28, 1358),
	(1, 1, 12),
	(1, 11, 1255),
	(1, 28, 1529),
	(1, 29, 616),
	(1, 7, 429),
	(1, 15, 102),
	(1, 17, 365),
	(1, 27, 948),
	(1, 5, 302),
	(1, 14, 489),
	(1, 4, 659),
	(1, 29, 891),
	(1, 4, 834),
	(1, 24, 1764),
	(1, 7, 285),
	(1, 22, 1536),
	(1, 4, 633),
	(1, 12, 1072),
	(1, 1, 585),
	(1, 4, 114),
	(1, 19, 726),
	(1, 6, 2211),
	(1, 25, 169),
	(1, 3, 975),
	(1, 10, 460),
	(1, 29, 980),
	(1, 34, 1067),
	(1, 35, 768),
	(1, 19, 569),
	(1, 11, 820),
	(1, 11, 503),
	(1, 17, 1083),
	(1, 22, 1007),
	(1, 5, 1491),
	(1, 19, 466),
	(1, 17, 839),
	(1, 18, 1227),
	(1, 31, 2680),
	(1, 7, 721),
	(1, 26, 439),
	(1, 28, 1926),
	(1, 25, 409),
	(1, 26, 1320),
	(1, 9, 1053),
	(1, 33, 1126),
	(1, 29, 498),
	(1, 3, 1733),
	(1, 12, 381);
/*!40000 ALTER TABLE `pokemon_user` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. types
DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.types : ~11 rows (environ)
DELETE FROM `types`;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `name`) VALUES
	(1, 'Plante'),
	(2, 'Feu'),
	(3, 'Eau'),
	(4, 'Insecte'),
	(5, 'Vol'),
	(6, 'Poison'),
	(7, 'Normal'),
	(8, 'Electrique'),
	(9, 'Sol'),
	(10, 'Fée');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Listage de la structure de la table pokemon_laravel. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pokeballs` bigint(20) NOT NULL,
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `catched` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table pokemon_laravel.users : ~1 rows (environ)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `password`, `pokeballs`, `level`, `catched`) VALUES
	(1, 'Admin', '$2y$10$CYkT0SYmoPj.QUO5fg9Cd.9BoQjlHWTy1OG7NVFwfdpvnfoGZbcMq', 274, 11, 27);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;