<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['type_id', 'start_time', 'end_time'];
    public $timestamps = false;
}
