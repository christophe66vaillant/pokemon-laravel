<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonUser extends Model
{
    protected $fillable = [
        'user_id', 'PC', 'pokemon_id',
    ];

    protected $table = 'pokemon_user';
    public $timestamps = false;
}
