<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    public function types()
    {
        return $this->belongsToMany(Type::class, 'pokemon_type');
    }
}
